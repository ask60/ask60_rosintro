#! /usr/bin/env python

import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg

# initialize moveit_commander and a rospy node:
moveit_commander.roscpp_initialize(sys.argv)
rospy.init_node('move_group_python_interface_tutorial', anonymous=True)

# Instantiate a RobotCommander object. Provides information such as the robot’s kinematic model and the robot’s current joint states
robot = moveit_commander.RobotCommander()
# Instantiate a PlanningSceneInterface object. This provides a remote interface for getting, setting, and updating the robot’s internal understanding of the surrounding world:
scene = moveit_commander.PlanningSceneInterface()    
# Instantiate a MoveGroupCommander object. This object is an interface to a planning group (group of joints).
move_group = moveit_commander.MoveGroupCommander("manipulator")

print("Reference frame: %s" % move_group.get_planning_frame())
print("End effector: %s" % move_group.get_end_effector_link())
print("Robot Groups:")
print(robot.get_group_names())
print("Current Joint Values: ")
print(move_group.get_current_joint_values())
print("Current Pose: ")
print(move_group.get_current_pose())
print("Robot State:")
print(robot.get_current_state())

scale = 1.0

# SET THE ROBOT TO A SPECIFIC INITIAL POSE
# We require setting a pose for future end effector movements. 
pose_goal = geometry_msgs.msg.Pose()
pose_goal.orientation.w = 1.0
pose_goal.position.x = 0.4
pose_goal.position.y = 0.4
pose_goal.position.z = 0.4

move_group.set_pose_target(pose_goal)

plan = move_group.go(wait=True)
# Calling `stop()` ensures that there is no residual movement
move_group.stop()
# It is always good to clear your targets after planning with poses.
# Note: there is no equivalent function for clear_joint_value_targets()
move_group.clear_pose_targets()

# We use a waypoints array to define a path for the end effector
# to follow and trace the initials. 
waypoints = []

# Get the current pose of the robot in order to modify the parameters
# for the cartesian path. 
wpose = move_group.get_current_pose().pose

# Make letter a
wpose.position.x -= scale * 0.1  # First move left
waypoints.append(copy.deepcopy(wpose))

wpose.position.z += scale * 0.1  # Then move up
waypoints.append(copy.deepcopy(wpose))

wpose.position.x += scale * 0.1  # Then move right
waypoints.append(copy.deepcopy(wpose))

wpose.position.z -= scale * 0.1  # Then move down
waypoints.append(copy.deepcopy(wpose))

wpose.position.x += scale * 0.03  # Then move a little right to finish A
waypoints.append(copy.deepcopy(wpose))

# RESET
wpose.position.x -= scale * 0.03  
waypoints.append(copy.deepcopy(wpose))

# Make letter s
wpose.position.x += scale * 0.1  # First move right
waypoints.append(copy.deepcopy(wpose))

wpose.position.z += scale * 0.1  # Then move up
waypoints.append(copy.deepcopy(wpose))

wpose.position.x -= scale * 0.1  # Then move left
waypoints.append(copy.deepcopy(wpose))

wpose.position.z += scale * 0.1  # Then move up
waypoints.append(copy.deepcopy(wpose))

wpose.position.x += scale * 0.1  # Finally move right to finish S
waypoints.append(copy.deepcopy(wpose))

# RESET
wpose.position.x -= scale * 0.1  # Reset X
wpose.position.z -= scale * 0.2  # Reset Z
waypoints.append(copy.deepcopy(wpose))

# Make letter k
wpose.position.z += scale * 0.1  # Move up 
waypoints.append(copy.deepcopy(wpose))

wpose.position.z -= scale * 0.05  # Move down half way
waypoints.append(copy.deepcopy(wpose))

wpose.position.z += scale * 0.05  # Move up
wpose.position.x += scale * 0.05  # and to the right
waypoints.append(copy.deepcopy(wpose))

wpose.position.z -= scale * 0.05  # reset
wpose.position.x -= scale * 0.05  # 
waypoints.append(copy.deepcopy(wpose))

wpose.position.z -= scale * 0.05  # Move down
wpose.position.x += scale * 0.05  # and to the right
waypoints.append(copy.deepcopy(wpose))

# We want the Cartesian path to be interpolated at a resolution of 1 cm
# which is why we will specify 0.01 as the eef_step in Cartesian
# translation.  We will disable the jump threshold by setting it to 0.0,
# ignoring the check for infeasible jumps in joint space, which is sufficient
# for this tutorial.
(plan, fraction) = move_group.compute_cartesian_path(
    waypoints, 0.01, 0.0  # waypoints to follow  # eef_step
)  # jump_threshold

move_group.execute(plan, wait=True)

moveit_commander.roscpp_shutdown()
